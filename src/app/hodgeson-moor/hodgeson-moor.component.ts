import { OrderService } from './../servises/order.service';
import { FirstCaseService } from './../servises/first-case.service';
import { EddService } from './../servises/edd.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hodgeson-moor',
  templateUrl: './hodgeson-moor.component.html',
  styleUrls: ['./hodgeson-moor.component.css']
})
export class HodgesonMoorComponent implements OnInit {

  constructor(private edd: EddService, private firstcase:FirstCaseService, 
              private orderservice: OrderService) { }

  hodgesonMoorArray = [];
  lateJobsArray = [];
  temp = 0;
  
  arrayToSort = this.firstcase.getFirstCase().slice();
  eddArray = [];
  
  ngOnInit() {
    this.eddArray = this.edd.eddSort();
    this.sortingEdd(this.eddArray);
    
    console.log("eddArray: ", this.eddArray);
    // console.log(this.eddArray)
  }

  sortingEdd(array){
    // this.eddArray = this.edd.eddSort();
    array.forEach(element => {
      // console.log(element.isLate);
      if(element.isLate){
        this.findLateJob(this.eddArray.indexOf(element));
        // console.log(lateJobIndex);
      }
    })
  }
  findLateJob(lateJobIndex: number){
    let oneArray = [];
    for(let index = 0; index <= lateJobIndex; index++){
      oneArray.push(this.eddArray[index]);
    }
    var max = oneArray.reduce(function (prev, current) {
      return (prev.processTime > current.processTime) ? prev : current
   });
   this.lateJobsArray.push(max);
  //  this.eddArray.splice(this.eddArray.indexOf(max,1))
   this.eddArray.splice(this.eddArray.indexOf(max), 1);
   console.log("lateJobsArray: ", this.lateJobsArray);
   this.eddArray = this.edd.eddSort();
  //  console.log("eddArray: ", this.eddArray);
    // console.log("oneArray: ", this.oneArray);
    // console.log("max: ", max);
  this.sortingEdd(this.eddArray);
  }
 
}
