import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FifoSummaryComponent } from './fifo-summary.component';

describe('FifoSummaryComponent', () => {
  let component: FifoSummaryComponent;
  let fixture: ComponentFixture<FifoSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FifoSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FifoSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
