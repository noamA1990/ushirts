import { FifoService } from './../servises/fifo.service';
import { SatisfactionService } from './../servises/satisfaction.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fifo-summary',
  templateUrl: './fifo-summary.component.html',
  styleUrls: ['./fifo-summary.component.css']
})
export class FifoSummaryComponent implements OnInit {
  fifoQWithSatisfaction;
  constructor(private s: SatisfactionService, private fifo: FifoService) { }

  ngOnInit() {
    this.fifoQWithSatisfaction = this.addSatisfaction(this.fifo.getQ())
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }
  
  addSatisfaction(array) {
    return this.s.claculateSatisfaction(array)
  }

  sumLateJobs(q) {
    let lateCount = 0;
    q.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness(q) {
    const latenssArr = [];
    let max = 0;
    q.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }


}
