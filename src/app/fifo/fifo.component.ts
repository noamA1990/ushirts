import { Component, OnInit, ViewChild, Injectable } from '@angular/core';
import { FifoService } from '../servises/fifo.service';
import { EddService } from '../servises/edd.service';
import { SatisfactionService } from '../servises/satisfaction.service';
import { FirstCaseService } from '../servises/first-case.service';
import { OrderService } from '../servises/order.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-fifo',
  templateUrl: './fifo.component.html',
  styleUrls: ['./fifo.component.css']
})
export class FifoComponent implements OnInit {
  displayedColumns = ['orderId', 'customer', 'type', 'qty', 'dd', 'fd', 'tardiness', 'vendor', 'cost', 'satisfaction'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  local = this.fifo.getLocal();
  china = this.fifo.getChina();
  queue = [];
  queueWithSatifaction = [];
  avgSatisfaction;
  constructor(private fifo: FifoService,
    private edd: EddService,
    private s: SatisfactionService,
    private firstCase: FirstCaseService,
    private order: OrderService) { }

  ngOnInit() {
    this.queue = this.fifo.getQ();
    this.queueWithSatifaction = this.s.claculateSatisfaction(this.queue);
    this.dataSource.data = this.queueWithSatifaction;
    this.dataSource.paginator = this.paginator;
    this.avgSatisfaction = (this.sum('satisfaction', this.queueWithSatifaction) / this.queueWithSatifaction.length);
  }

  sum(key, array) {
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }

  sumLateJobs() {
    let lateCount = 0;
    this.queue.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness() {
    const latenssArr = [];
    let max = 0;
    this.queue.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  maxSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

  minSatisfaction() {
    const satisfactionArr = [];
    let max = 0;
    this.queue.forEach(el => {
      satisfactionArr.push(el.satisfaction);
    });
    if (satisfactionArr.length === 0) {
      max = 0;
    } else {
      max = satisfactionArr.reduce((a, b) => {
        return Math.min(a, b);
      });
    }
    return max;
  }

}
