import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './nav/header/header.component';
import { SidenavListComponent } from './nav/sidenav-list/sidenav-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { FifoComponent } from './fifo/fifo.component';
import { EddComponent } from './edd/edd.component';
import { FormComponent } from './form/form.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SatisfactionService } from './servises/satisfaction.service';
import { FormService } from './servises/form.service';
import { OrderService } from './servises/order.service';
import { VendorsService } from './servises/vendors.service';
import { FifoService } from './servises/fifo.service';
import { FirstCaseService } from './servises/first-case.service';
import { EddService } from './servises/edd.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FifoSummaryComponent } from './fifo-summary/fifo-summary.component';
import { EddSummaryComponent } from './edd-summary/edd-summary.component';
import { HodgesonMoorComponent } from './hodgeson-moor/hodgeson-moor.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavListComponent,
    FifoComponent,
    EddComponent,
    FormComponent,
    WelcomeComponent,
    DashboardComponent,
    FifoSummaryComponent,
    EddSummaryComponent,
    HodgesonMoorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [    
    FormService,
    OrderService,
    VendorsService,
    FifoService,
    FirstCaseService,
    EddService,
    // DashboardService,
    // HodgesonMoorService,
    SatisfactionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
