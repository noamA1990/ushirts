import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  url = false;
  @Output() sidenavToggle = new EventEmitter<void>();

  constructor(private router: Router) {
    router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {

        if (event.url === '/' || event.url === '/welcome') {
          this.url = true;
        } else {
          this.url = false;
        }
      }
    });
  }

  ngOnInit() {
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

home() {
  this.router.navigateByUrl('/welcome');
}
}
