import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SatisfactionService {

  constructor() { }

  claculateSatisfaction(queue) {
    const q = queue.slice();
    q.forEach(order => {
      let satisfaction = 1;
      if (order.isLate) {
        satisfaction -= (order.daysLate * 0.01);
      } else {
        satisfaction -= ((order.daysEarly / 7) * 0.01);
      }

      satisfaction < 0 ? satisfaction = 0 : '';
      order.satisfaction = +satisfaction.toFixed(2);

    });
    return q;
  }
}
