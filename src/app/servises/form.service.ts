import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor() { }
  
  getShirtType() {
    return [
      { type: 'American T-Shirt' },
      { type: 'T-Shirt' },
      { type: 'Dress Shirt' },
      { type: 'Polo Shirt' },
    ];
  }

  getShirtColor() {
    return [
      { color: 'Black' },
      { color: 'White' },
      { color: 'Grey' },
      { color: 'Blue' },
      { color: 'Light  Blue'},
      { color: 'Red' },
      { color: 'Yellow' },
    ];
  }

  getSleeveColor() {
    return [
      { color: 'Black' },
      { color: 'Blue' },
      { color: 'White' },
      { color: 'Yellow' },
    ];
  }

  getPreferences() {
    return [
      { description: 'None' },
      { description: 'flexible on colors for quicker delivery' },
      { description: 'flexible on qantity & colors for lower price' },
      { description: 'flexible on delivery date for lower price' },
      { description: 'bigger order quantity for lower price per unit' },
      { description: 'flexible on delivery date for lower price per unit' },
      { description: 'flexible on colors for lower price per unit' },
    ];
  }

  getDeliveryRange() {
    return [
      { quarter: 'First Quarter', min: 1, max: 3 },
      { quarter: 'Second Quarter', min: 4, max: 6 },
      { quarter: 'Third Quarter', min: 7, max: 9  },
      { quarter: 'Fourth Quarter', min: 10, max: 12  },
      { quarter: 'First Half', min: 1, max: 6 },
      { quarter: 'Second Half', min: 7, max: 12  },
      { quarter: 'Flexible', min: 1, max: 12 },
    ];
  }

  getExtraFeatures() {
    return [
      { name: 'None'},
      { name: 'Right Pocket'},
      { name: 'Left Pocket'},
      { name: 'Two Pockets'},
      { name: 'Collar Buttons'}
    ];
  }

  getLocal() {
    return [
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Black', cost: 10, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'White', cost: 10, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Grey', cost: 12, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Blue', cost: 12, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Light Blue', cost: 13, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Red', cost: 4, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Yellow', cost: 14, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Black', cost: 10, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'White', cost: 10, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Grey', cost: 12, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Blue', cost: 12, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Light Blue', cost: 13, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Red', cost: 4, processTime: 7 },
      { isAmerican: true , minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Yellow', cost: 14, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Black', cost: 12, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'White', cost: 12, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Grey', cost: 14, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Blue', cost: 14, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Light Blue', cost: 15, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Red', cost: 16, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Yellow', cost: 16, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Black', cost: 17, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'White', cost: 17, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Grey', cost: 19, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Blue', cost: 19, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Light Blue', cost: 20, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Red', cost: 21, processTime: 7 },
      { isAmerican: false , minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Yellow', cost: 21, processTime: 7 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Black' , cost: 7, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'White' , cost: 7, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Grey' , cost: 9, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Blue' , cost: 9, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Light Blue' , cost: 10, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Red' , cost: 11, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Yellow' , cost: 11, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Black' , cost: 7, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'White' , cost: 7, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Grey' , cost: 9, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Blue' , cost: 9, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Light Blue' , cost: 10, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Red' , cost: 11, processTime: 30 },
      { isAmerican: true , minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Yellow' , cost: 11, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Black', cost: 8, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'White', cost: 8, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Grey', cost: 10, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Blue', cost: 10, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Light Blue', cost: 11, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Red', cost: 12, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Yellow', cost: 12, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Black', cost: 13, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'White', cost: 13, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Grey', cost: 15, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Blue', cost: 15, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Light Blue', cost: 16, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Red', cost: 17, processTime: 30 },
      { isAmerican: false , minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Yellow', cost: 17, processTime: 30 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'White', cost: 4, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Black', cost: 4, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Grey', cost: 6, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Blue', cost: 6, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Light Blue', cost: 7, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Red', cost: 8, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Yellow', cost: 8, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'White', cost: 4, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Black', cost: 4, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Grey', cost: 6, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Blue', cost: 6, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Light Blue', cost: 7, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Red', cost: 8, processTime: 50 },
      { isAmerican: true , minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Yellow', cost: 8, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Black', cost: 6, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'White', cost: 6, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Grey', cost: 8, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Blue', cost: 8, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Light Blue', cost: 9, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Red', cost: 10, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Yellow', cost: 10, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Black', cost: 12, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'White', cost: 12, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Grey', cost: 14, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Blue', cost: 14, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Light Blue', cost: 15, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Red', cost: 16, processTime: 50 },
      { isAmerican: false , minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Yellow', cost: 16, processTime: 50 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Black', cost: 3, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'White', cost: 3, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Grey', cost: 5, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Blue', cost: 5, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Light Blue', cost: 6, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Red', cost: 7, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Yellow', cost: 7, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Black', cost: 3, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'White', cost: 3, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Grey', cost: 5, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Blue', cost: 5, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Light Blue', cost: 6, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Red', cost: 7, processTime: 40 },
      { isAmerican: true , minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Yellow', cost: 7, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Black', cost: 5, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'White', cost: 5, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Grey', cost: 7, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Blue', cost: 7, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Light Blue', cost: 8, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Red', cost: 9, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Yellow', cost: 9, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Black', cost: 10, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'White', cost: 10, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Grey', cost: 12, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Blue', cost: 12, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Light Blue', cost: 13, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Red', cost: 14, processTime: 40 },
      { isAmerican: false , minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Yellow', cost: 14, processTime: 40 },
    ];
  }

  getLocalExtras() {
    return [
      { type: 'None', cost: 0 },
      { type: 'Right Pocket', cost: 2 },
      { type: 'Left Pocket', cost: 3 },
      { type: 'Two Pockets', cost: 5 },
      { type: 'Collar Buttons', cost: 1 }
    ];
  }

  getLocalAmerican() {
    return [
      { body: 'White', sleeves: 'Black', cost: 0 },
      { body: 'White', sleeves: 'Blue', cost: 2 },
      { body: 'Blue', sleeves: 'White', cost: 2 },
      { body: 'Red', sleeves: 'Yellow', cost: 8 },
    ];
  }

  getChinaExtras() {
    return [
      { type: 'None', cost: 0 },
      { type: 'Right Pocket', cost: 1 },
      { type: 'Left Pocket', cost: 2 },
      { type: 'Two Pockets', cost: 3 },
      { type: 'Collar Buttons', cost: 1 }
    ];
  }

  getChina() {
    return [
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Black', cost: 7, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'White', cost: 7, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Grey', cost: 8, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Blue', cost: 8, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Light Blue', cost: 9, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Red', cost: 10, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'T-Shirt', color: 'Yellow', cost: 11, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Black', cost: 7, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'White', cost: 7, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Grey', cost: 8, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Blue', cost: 8, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Light Blue', cost: 9, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Red', cost: 10, processTime: 6 },
      {  isAmerican: true, minQty: 1, maxQty: 1000, type: 'American T-Shirt', color: 'Yellow', cost: 11, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Black', cost: 10, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'White', cost: 10, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Grey', cost: 11, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Blue', cost: 11, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Light Blue', cost: 12, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Red', cost: 13, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Polo Shirt', color: 'Yellow', cost: 14, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Black', cost: 15, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'White', cost: 15, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Grey', cost: 16, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Blue', cost: 16, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Light Blue', cost: 17, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Red', cost: 18, processTime: 6 },
      {  isAmerican: false, minQty: 1, maxQty: 1000, type: 'Dress Shirt', color: 'Yellow', cost: 19, processTime: 6 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Black' , cost: 5, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'White' , cost: 5, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Grey' , cost: 6, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Blue' , cost: 6, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Light Blue' , cost: 7, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Red' , cost: 8, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'T-Shirt', color: 'Yellow' , cost: 9, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Black' , cost: 5, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'White' , cost: 5, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Grey' , cost: 6, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Blue' , cost: 6, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Light Blue' , cost: 7, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Red' , cost: 8, processTime: 18 },
      {  isAmerican: true, minQty: 1001, maxQty: 5000, type: 'American T-Shirt', color: 'Yellow' , cost: 9, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Black', cost: 6, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'White', cost: 6, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Grey', cost: 7, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Blue', cost: 7, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Light Blue', cost: 8, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Red', cost: 9, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Polo Shirt', color: 'Yellow', cost: 10, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Black', cost: 10, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'White', cost: 10, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Grey', cost: 11, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Blue', cost: 11, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Light Blue', cost: 12, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Red', cost: 13, processTime: 18 },
      {  isAmerican: false, minQty: 1001, maxQty: 5000, type: 'Dress Shirt', color: 'Yellow', cost: 14, processTime: 18 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'White', cost: 3, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Black', cost: 3, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Grey', cost: 4, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Blue', cost: 4, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Light Blue', cost: 5, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Red', cost: 6, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'T-Shirt', color: 'Yellow', cost: 7, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Black', cost: 3, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Grey', cost: 4, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Blue', cost: 4, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Light Blue', cost: 5, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Red', cost: 6, processTime: 23 },
      {  isAmerican: true, minQty: 5001, maxQty: 10000, type: 'American T-Shirt', color: 'Yellow', cost: 7, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Black', cost: 4, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'White', cost: 4, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Grey', cost: 5, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Blue', cost: 5, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Light Blue', cost: 6, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Red', cost: 7, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Polo Shirt', color: 'Yellow', cost: 8, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Black', cost: 9, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'White', cost: 9, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Grey', cost: 10, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Blue', cost: 10, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Light Blue', cost: 11, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Red', cost: 12, processTime: 23 },
      {  isAmerican: false, minQty: 5001, maxQty: 10000, type: 'Dress Shirt', color: 'Yellow', cost: 13, processTime: 23 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Black', cost: 1, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'White', cost: 1, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Grey', cost: 2, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Blue', cost: 2, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Light Blue', cost: 3, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Red', cost: 4, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'T-Shirt', color: 'Yellow', cost: 5, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Black', cost: 1, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'White', cost: 1, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Grey', cost: 2, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Blue', cost: 2, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Light Blue', cost: 3, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Red', cost: 4, processTime: 30 },
      {  isAmerican: true, minQty: 10001, maxQty: 150000, type: 'American T-Shirt', color: 'Yellow', cost: 5, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Black', cost: 3, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'White', cost: 3, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Grey', cost: 4, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Blue', cost: 4, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Light Blue', cost: 5, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Red', cost: 6, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Polo Shirt', color: 'Yellow', cost: 7, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Black', cost: 7, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'White', cost: 7, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Grey', cost: 8, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Blue', cost: 8, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Light Blue', cost: 9, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Red', cost: 10, processTime: 30 },
      {  isAmerican: false, minQty: 10001, maxQty: 150000, type: 'Dress Shirt', color: 'Yellow', cost: 11, processTime: 30 },
    ];
  }
}

