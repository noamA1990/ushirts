import { Injectable } from '@angular/core';
import { FormService } from './form.service';
import { OrderService } from './order.service';
import { FirstCaseService } from './first-case.service';

@Injectable({
  providedIn: 'root'
})
export class EddService {
  queue = [];
  localQ = [];
  chinaQ = [];
  local = this.formService.getLocal();
  china = this.formService.getChina();
  eddQ = [];
  newQ = [];
  queueWithConstraints = [];

  min;
  max;
  lateness;
  totalLateness;
  cost;

  constructor(
    private formService: FormService,
    private orders: OrderService,
    private firstCase: FirstCaseService
  ) { }

  getmin() {
    return this.min;
  }

  getmax() {
    return this.max;
  }

  getlatedays() {
    return this.lateness;
  }

  gettotallate() {
    return this.totalLateness;
  }

  getCost() {
    return this.cost;
  }

  setEddQ(q) {
    this.eddQ = q;
  }

  getStartingEddQ() {
    return this.eddQ;
  }

  getEddQ() {
    return this.queue;
  }

  setStartingQueue(queue) {
    this.queue = [...queue];
  }

  getStartingQueue() {
    return this.queue;
  }

  eddSort() {
    // let arrayToSort;
    let arrayToSort = [];
    this.queue = this.firstCase.getFirstCase().slice();
    // console.log(this.queue[0])
    if (this.queueWithConstraints.length === 0) {
      for (let i = 0; i < this.queue.length; i++) {
        if (this.queue[i].preferences !== 'None') {

          this.queueWithConstraints.push(this.queue[i])
        }
        // console.log(this.queueWithConstraints)
      }
    }

    if (this.queueWithConstraints.length !== 0) {

      for (let index = 0; index < this.queueWithConstraints.length; index++) {
        if (this.queueWithConstraints[index].preferences === 'flexible on colors for quicker delivery') {
          this.queue.forEach(order => {
            if (order.joinrdCustomer === false) {
              if (order.shirtType === this.queueWithConstraints[index].shirtType) {
                let orderExtraFeatures = order.extraFeatures.join(' ');
                let elementExtraFeatures = this.queueWithConstraints[index].extraFeatures.join(' ');
                if (orderExtraFeatures === elementExtraFeatures && order.customer !== this.queueWithConstraints[index].customer) {
                  if (order.desiredDeliveryDate <= this.queueWithConstraints[index].desiredDeliveryDate) {
                    order.customer = [`${this.queueWithConstraints[index].customer}`, `${order.customer}`];
                    order.qty += this.queueWithConstraints[index].qty;
                    order.joinrdCustomer = true
                    this.queueWithConstraints[index].joinrdCustomer = true
                     this.queue.splice(this.queue.indexOf(this.queueWithConstraints[index]), 1);
                     // console.log(`${this.queueWithConstraints[index].customer}`, `${order.customer}`)
                    }
                  }
                }
              }
              
            })
            // arrayToSort = this.queue.slice()

          
        }
        if (this.queueWithConstraints[index].preferences === 'flexible on qantity & colors for lower price') {
          this.queue.forEach(order => {
            if (order.joinrdCustomer === false) {
              if (order.shirtType === this.queueWithConstraints[index].shirtType) {
                let orderExtraFeatures = order.extraFeatures.join(' ');
                let elementExtraFeatures = this.queueWithConstraints[index].extraFeatures.join(' ');
                if (orderExtraFeatures === elementExtraFeatures && order.customer !== this.queueWithConstraints[index].customer) {
                  if ((order.qty + this.queueWithConstraints[index].qty) >= 1000 ||
                    (order.qty + this.queueWithConstraints[index].qty) >= 5000 ||
                    (order.qty + this.queueWithConstraints[index].qty) >= 10000) {
                    if (order.desiredDeliveryDate <= this.queueWithConstraints[index].desiredDeliveryDate) {
                      order.customer = [`${this.queueWithConstraints[index].customer}`, `${order.customer}`];
                      order.qty += this.queueWithConstraints[index].qty;
                      order.joinrdCustomer = true
                      this.queue.splice(this.queue.indexOf(this.queueWithConstraints[index]), 1);
                      // console.log(`${this.queueWithConstraints[index].customer}`, `${order.customer}`)
                    }
                  }
                }
              }
            }
           
          })
        }
      }
    }
    // console.log(this.queue)
    
    this.queue.forEach(element => {
      // console.log(Array.isArray(element.customer))
      if(Array.isArray(element.customer) === false) {
        if(!element.joinrdCustomer){
          arrayToSort.push(element)
        }
      }
      else if(Array.isArray(element.customer)){
        arrayToSort.push(element)
      }
    })
    // console.log(arrayToSort)
    // let arrayToSort = this.queue.slice();
    arrayToSort.sort((a, b) => a.desiredDeliveryDate - b.desiredDeliveryDate);
    this.orders.determineCosts(arrayToSort, this.chinaQ, this.localQ)
    return arrayToSort;
  }

  setVendorQueue(Q) {
    Q.forEach(el => {
      if (el == null) {

      } else {
        el.vendor === 'local' ? this.localQ.push(el) : this.chinaQ.push(el);
      }
    });
  }

  setFifoData(min, max, lateness, totalLateness, cost) {
    this.min = min;
    this.max = max;
    this.lateness = lateness;
    this.totalLateness = totalLateness;
    this.cost = cost;
  }
}
