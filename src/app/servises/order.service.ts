import { Injectable } from '@angular/core';
import { VendorsService } from './vendors.service';
import { FormService } from './form.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  queue = [];
  china = [];
  local = [];

  constructor(private vendorService: VendorsService, private formService: FormService) { }

  determineCosts(queue, chinaQ: any[], localQ) {
    // console.log(queue)
    let localAmerican;
    let localRegular = 0;
    let localExtras = 0;
    let chinaAmerican = 0;
    let chinaRegular = 0;
    let chinaExtras = 0;
    let totalLocal = 0;
    let totalChineese = 0;
    let localRegularRes;
    let chinaRegularRes;
    let localPPU = 0;
    let chinaPPU = 0;
    let localAmericanCost = 0;
    let localAmericanPPU = 0;
    let cPT = 0;
    let lPT = 0;
    let forecastDelivery;

    queue.forEach(data => {
      let flag = false;
      let topTier = 0;
      if (data.cost === 0) {
        // if (data.preferences === 'flexible on qantity & colors for lower price' && flag === false) {
        //   this.formService.getChina().forEach(venEl => {
        //     if ((venEl.maxQty - data.qty) <= (venEl.maxQty *0.2) && data.shirtType === venEl.type
        //     && data.bodyColor === venEl.color){
        //         flag = true;
        //         // topTier = venEl.maxQty;
        //         data.qty = venEl.maxQty +1;
        //     }
            
        //   });
        //   // data.qty = (((topTier - data.qty) + data.qty + 1))
        // }
        if (data.shirtType === 'American T-Shirt') {
          localAmerican = this.vendorService.localAmericanCost(data);
          chinaAmerican = this.vendorService.chinaAmericanCost(data);
          localAmericanCost = localAmerican.cost;
          localAmericanPPU = localAmerican.ppu;
        }

        localExtras = this.vendorService.extrasCost(data, this.formService.getLocalExtras());
        chinaExtras = this.vendorService.extrasCost(data, this.formService.getChinaExtras());
        localRegularRes = this.vendorService.regularCost(data, this.formService.getLocal());
        chinaRegularRes = this.vendorService.regularCost(data, this.formService.getChina());
        localRegular = localRegularRes.cost;
        chinaRegular = chinaRegularRes.cost;
        localPPU = localRegularRes.ppu;
        chinaPPU = chinaRegularRes.ppu;

        totalLocal = localAmericanCost + localRegular + localExtras;
        totalChineese = chinaAmerican + chinaRegular + chinaExtras;

        if (totalLocal > totalChineese) {
          data.cost = totalChineese;
          data.pricePerUnit = chinaPPU;
          data.vendor = 'china';
          chinaQ.forEach(order => {
            if (order.barcode !== data.barcode && order.customer === data.customer) {
              chinaQ.push(data);
            }
          })
        } else {
          data.cost = totalLocal;
          data.pricePerUnit = localPPU;
          data.vendor = 'local';
          localQ.forEach(order => {
            if (order.barcode !== data.barcode && order.customer !== data.customer) {
              localQ.push(data);
            }
          })
        }

        data.cCost = totalChineese;
        data.lCost = totalLocal;
        data.ppUChina = chinaPPU;
        data.ppULocal = localPPU;

      } else {
        data = data;
      }
      this.addProcessTime(data);
      if (data.vendor === 'china') {
        cPT += data.processTime;
        forecastDelivery = this.addDays(data.dateRecieved, cPT)
      }
      else {
        lPT += data.processTime;
        forecastDelivery = this.addDays(data.dateRecieved, lPT)
      }
      data.forecastDeliveryDate = forecastDelivery

      const date = this.checkTardiness(data.forecastDeliveryDate, data.desiredDeliveryDate);
      if (data.forecastDeliveryDate > data.desiredDeliveryDate) {
        data.daysLate = date * -1;
        data.daysEarly = 0;
        data.isLate = true;
      } else {
        data.daysLate = 0;
        data.daysEarly = date;
        data.isLate = false;
      }
    });
    return queue;
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  addProcessTime(order) {
    if (order.vendor === 'china') {
      this.formService.getChina().forEach(venEl => {
        if (order.qty >= venEl.minQty &&
          order.qty <= venEl.maxQty &&
          order.shirtType === venEl.type
          && order.bodyColor === venEl.color) {
          order.processTime = venEl.processTime;
        }
      })
    }
    else {
      this.formService.getLocal().forEach(venEl => {
        if (order.qty >= venEl.minQty &&
          order.qty <= venEl.maxQty &&
          order.shirtType === venEl.type
          && order.bodyColor === venEl.color) {
          order.processTime = venEl.processTime;
        }
      })
    }
  }

  checkTardiness(fd: Date, dd: Date) {
    fd = new Date(fd);
    dd = new Date(dd);
    return Math.floor(
      (Date.UTC(dd.getFullYear(), dd.getMonth(), dd.getDate())
        - Date.UTC(fd.getFullYear(), fd.getMonth(), fd.getDate())) / (1000 * 60 * 60 * 24));
  }
}
