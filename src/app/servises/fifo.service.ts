import { Injectable } from '@angular/core';
import { FirstCaseService } from './first-case.service';
import { OrderService } from './order.service';

@Injectable({
  providedIn: 'root'
})
export class FifoService {

  constructor(private firstCase: FirstCaseService, private orders: OrderService) { }
  queue = [];
  china = [];
  local = [];
  

  storeAllDataLocaly(queue, china, local) {
    this.queue = [...queue];
    this.local = [...local];
    this.china = [...china];
  }
  getQ() {
    this.queue = this.firstCase.getFirstCase().slice();
    this.queue.sort((a, b) => a.dateRecieved - b.dateRecieved);
    this.orders.determineCosts(this.queue, this.china, this.local);
    return this.queue;
  }

  

  getChina() {
    return this.china;
  }

  getLocal() {
    return this.local;
  }
}
