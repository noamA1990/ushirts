import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FifoComponent } from './fifo/fifo.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FormComponent } from './form/form.component';
import { EddComponent } from './edd/edd.component';
import { HodgesonMoorComponent } from './hodgeson-moor/hodgeson-moor.component';


const routes: Routes = [
  { path: 'form', component: FormComponent },
  { path: 'fifo', component: FifoComponent },
  { path: 'edd', component: EddComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'hm', component: HodgesonMoorComponent },
  { path: 'welcome', component: WelcomeComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
