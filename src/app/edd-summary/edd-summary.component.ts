import { EddService } from './../servises/edd.service';
import { SatisfactionService } from './../servises/satisfaction.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edd-summary',
  templateUrl: './edd-summary.component.html',
  styleUrls: ['./edd-summary.component.css']
})
export class EddSummaryComponent implements OnInit {
  eddQWithSatisfaction
  constructor(private s: SatisfactionService, private edd: EddService) { }

  ngOnInit() {
    this.eddQWithSatisfaction = this.addSatisfaction(this.edd.eddSort())

  }

  sum(key, array) {
    const arr = array.slice();
    return arr.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }
  
  addSatisfaction(array) {
    return this.s.claculateSatisfaction(array)
  }

  sumLateJobs(q) {
    const a = q.slice();
    let lateCount = 0;
    a.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness(q) {
    const a = q.slice();
    const latenssArr = [];
    let max = 0;
    a.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }

}
