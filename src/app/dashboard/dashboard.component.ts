import { EddService } from './../servises/edd.service';
import { SatisfactionService } from './../servises/satisfaction.service';
import { FifoService } from './../servises/fifo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  fifoQWithSatisfaction;
  eddQWithSatisfaction;
  eddSorted;
  fifoTC;
  fifoL;
  fifoML;
  fifoAvgS;
  eddTC;
  eddL;
  eddML;
  eddAvgS;

  constructor(
    private s: SatisfactionService,
    private fifo: FifoService,
    private edd: EddService,
  ) { }

  ngOnInit() {
    this.fifoQWithSatisfaction = this.addSatisfaction(this.fifo.getQ())
    this.eddQWithSatisfaction = this.addSatisfaction(this.edd.eddSort())
    this.fifoTC = this.sum('cost', this.fifoQWithSatisfaction)
    this.fifoL = this.sumLateJobs(this.fifoQWithSatisfaction)
    this.eddTC = this.sum('cost', this.eddQWithSatisfaction)
    this.eddL = this.sumLateJobs(this.eddQWithSatisfaction)
    // console.log(this.eddQWithSatisfaction);
  }

  sum(key, array) {
    // console.log(array)
    return array.reduce((a, b) => a + (b[key] || 0), 0);
  }

  sumAvg(key, array) {
    return this.sum(key, array) / array.length;
  }

  addSatisfaction(array) {
    const res = this.claculateSatisfaction(array)
    return res;
  }

  sumLateJobs(q) {
    let lateCount = 0;
    q.forEach(el => {
      if (el.isLate) {
        lateCount += 1;
      }
    });
    return lateCount;
  }

  maxLateness(q) {
    const latenssArr = [];
    let max = 0;
    q.forEach(el => {
      latenssArr.push(el.daysLate);
    });
    if (latenssArr.length === 0) {
      max = 0;
    } else {
      max = latenssArr.reduce((a, b) => {
        return Math.max(a, b);
      });
    }
    return max;
  }
  claculateSatisfaction(queue) {
    const q = queue.slice();
    q.forEach(order => {
      let satisfaction = 1;
      if (order.isLate) {
        satisfaction -= (order.daysLate * 0.01);
      } else {
        satisfaction -= ((order.daysEarly / 7) * 0.01);
      }

      satisfaction < 0 ? satisfaction = 0 : '';
      order.satisfaction = +satisfaction.toFixed(2);

    });
    return q;
  }
}
